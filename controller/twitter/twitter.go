package twitter

import (
	"github.com/gorilla/websocket"	
	"io/ioutil"
	"net/http"
	"time"
	"sync"
	"log"	
)

type Twitter struct {	
	Client *websocket.Conn
	Master *websocket.Conn
	buffer [][]byte
	mux *sync.Mutex
}

func New() *Twitter {
	twitter := Twitter{
		mux: &sync.Mutex{},
	}

	return &twitter
}

func (t *Twitter) ConnectMaster(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r, w.Header(), 1024, 1024)
	if err != nil {
		http.Error(w, err.Error(), 500)
		log.Println(err.Error())

		return
	}

	t.Master = conn
	w.WriteHeader(http.StatusSwitchingProtocols)

	// go t.pingPongMaster()
	go t.readFromMaster()
	go t.readBuffer()
}

func (t *Twitter) ConnectClient(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r, w.Header(), 1024, 1024)
	if err != nil {
		http.Error(w, err.Error(), 500)
		log.Println(err.Error())

		return
	}

	t.Client = conn
	go t.pingPongClient()
	w.WriteHeader(http.StatusSwitchingProtocols)
}

func (t *Twitter) readFromMaster() {	
	for {
		_, r, err := t.Master.NextReader()
		if err != nil {
			log.Println(err.Error())
			t.Master = nil
			return
		}

		msg, err := ioutil.ReadAll(r)
		if err != nil {
			log.Println(err.Error())
		}

		if t.Client != nil {
			t.mux.Lock()
			if err := t.Client.WriteMessage(1, msg); err != nil {
				log.Println(err.Error())
				t.buffer = append(t.buffer, msg)
			}

			t.mux.Unlock()

		} else {
			t.mux.Lock()
			t.buffer = append(t.buffer, msg)
			t.mux.Unlock()
		}
	}
}

// func (t *Twitter) pingPongMaster() {
// 	pongWait := 20 * time.Second
// 	pingPeriod := (pongWait * 9) / 10
// 	t.Master.SetReadDeadline(time.Now().Add(pongWait))
// 	t.Master.SetPongHandler(func(string) error {
// 		t.Master.SetReadDeadline(time.Now().Add(pongWait))
// 		log.Println("master: pong received")

// 		return nil
// 	})

// 	ticker := time.NewTicker(pingPeriod)

// 	defer func() {
// 		ticker.Stop()
// 		t.Master.Close()
// 	}()

// 	for {
// 		<- ticker.C
// 		log.Println("write ping to master")
// 		if err := t.Master.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
// 			log.Println(err.Error())

// 			return
// 		}
// 	}
// }

func (t *Twitter) pingPongClient() {
	pongWait := 20 * time.Second
	pingPeriod := (pongWait * 9) / 10
	t.Client.SetReadDeadline(time.Now().Add(pongWait))
	t.Client.SetPongHandler(func(string) error {
		t.Client.SetReadDeadline(time.Now().Add(pongWait))
		log.Println("client: pong received")

		return nil
	})

	ticker := time.NewTicker(pingPeriod)

	defer func() {
		ticker.Stop()
		t.Client.Close()
	}()

	go func(){
		for {
			if _, _, err := t.Client.NextReader(); err != nil {
				log.Println(err.Error())

				return
			}
		}
	}()

	for {
		<- ticker.C
		t.mux.Lock()
		log.Println("write ping to client")
		if err := t.Client.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
			log.Println(err.Error())
			t.mux.Unlock()

			return
		}
		
		t.mux.Unlock()
	}
}

func (t *Twitter) readBuffer() {
	for {
		if len(t.buffer) > 0 {
			if t.Client != nil {
				t.mux.Lock()
				msg := t.buffer[0]
				if err := t.Client.WriteMessage(1, msg); err != nil {
					log.Println(err.Error())
					t.Client = nil
				} else {
					t.buffer = append(t.buffer[:0], t.buffer[1:]...)
				}

				t.mux.Unlock()
			}
		}
	}
}
