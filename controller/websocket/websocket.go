package websocket

import (
	"github.com/gorilla/websocket"	
	"collin/spider/util"
	"collin/broadcast"	
	"encoding/json"
	"net/http"
	"strconv"
	"time"
	"sync"
	"log"

	"BagiDataSocket/database"
)

const (
	LIKE_VIDEO = "like-video"
	LIKE_COMMENT = "like-comment"
	DISLIKE_VIDEO = "dislike-video"
	DISLIKE_COMMENT = "dislike-comment"
	CREATE_COMMENT = "create-comment"
	UPDATE_COMMENT = "update-comment"

	MSG_INFO = "info"
	MSG_ERROR = "error"
	MSG_INTERNAL_ERROR = "internal-error"
	MSG_LIKE_VIDEO = "like-video"
	MSG_DISLIKE_VIDEO = "dislike-video"
	MSG_LIKE_COMMENT = "like-comment"
	MSG_DISLIKE_COMMENT = "dislike-comment"
	MSG_UPDATE_COMMENT = "update-comment"
	MSG_CREATE_COMMENT = "create-comment"
)

type Socket struct {
	Broadcast *broadcast.Broadcast
	mux *sync.Mutex
	DB *database.DB
}

type Worker struct {
	id string
	videoId string
	mux *sync.Mutex
	conn *websocket.Conn
	DB *database.DB
	broadcast *broadcast.Chan
	exit chan bool
}

type Command struct {
	Tag string `json:"tag,omitempty"`
	VideoId string `json:"videoId,omitempty"`
	CommentId string `json:"commentId,omitempty"`
	Command string `json:"command,omitempty"`
	Data string `json:"data,omitempty"`
}

type Message struct {
	Tag string `json:"tag,omitempty"`
	Type string `json:"type,omitempty"`
	Message string `json:"message,omitempty"`
	Data *MessageData `json:"data,omitempty"`
}

type MessageData struct {
	VideoId string `json:"videoId,omitempty"`
	CommentId string `json:"commentId,omitempty"`
	LikeCount string `json:"likeCount,omitempty"`
	DislikeCount string `json:"disLikeCount,omitempty"`
	UpdatedText string `json:"updatedText,omitempty"`
	Text string `json:"text,omitempty"`
}

type Comment struct {
	Id string `json:"id"`
	UserId string `json:"userId"`
	VideoId string `json:"videoId"`
	Text string `json:"text"`
	LikeCount string `json:"likeCount"`
	DislikeCount string `json:"dislikeCount"`
	CreatedAt string `json:"createdAt"`
	UpdatedAt string `json:"updatedAt"`
}

type Video struct {
	Id string `json:"id"`
	Title string `json:"title"`
	FileDir string `json:"fileDir"`
	Description string `json:"description"`
	LikeCount string `json:"likeCount"`
	DislikeCount string `json:"dislikeCount"`
	CreatedAt string `json:"createdAt"`
	UpdatedAt string `json:"updatedAt"`
	Comments *[]Comment `json:"comments"`
}

func (s *Socket) VideoData(w http.ResponseWriter, r *http.Request) {
	video := Video{}
	getVideoData := s.DB.Conn["bagidata"].Table("video").
		Select("video_id,title,file_dir,description,like_count,dislike_count,created_at,updated_at").
		Where("video_id", "=", r.URL.Query().Get("videoId")).
		GetRow()	

	err := getVideoData.Scan(
		&video.Id,
		&video.Title,
		&video.FileDir,
		&video.Description,
		&video.LikeCount,
		&video.DislikeCount,
		&video.CreatedAt,
		&video.UpdatedAt,
	)

	if err != nil {
		http.Error(w, "video not found", 404)
		return
	}

	comments := []Comment{}
	getComments := s.DB.Conn["bagidata"].Table("comment").
		Select("comment_id,user_id,video_id,comment_text,like_count,dislike_count,created_at,updated_at").
		Where("video_id", "=", video.Id).
		Get()
	if getComments.Error != nil {
		http.Error(w, getComments.Error.Error(), 500)
		return
	}

	for getComments.Rows.Next() {
		comm := Comment{}
		err := getComments.Rows.Scan(
			&comm.Id,
			&comm.UserId,
			&comm.VideoId,
			&comm.Text,
			&comm.LikeCount,
			&comm.DislikeCount,
			&comm.CreatedAt,
			&comm.UpdatedAt,
		)

		if err != nil {
			log.Println(err.Error())
			break
		}
	}

	if len(comments) > 0 {
		video.Comments = &comments
	}	

	json.NewEncoder(w).Encode(video)
}

func (s *Socket) Connect(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r, w.Header(), 1024, 1024)
	if err != nil {
		http.Error(w, err.Error(), 500)
		log.Println(err.Error())

		return
	}
	
	worker := &Worker {
		id: r.Header.Get("id"),
		videoId: r.URL.Query().Get("videoId"),
		mux: &sync.Mutex{},
		conn: conn,
		DB: s.DB,
		broadcast: s.Broadcast.Open(),
		exit: make(chan bool),
	}

	go worker.socketReadWrite()
}

func (w *Worker) socketReadWrite() {
	exit := make(chan bool)

	pongWait := 20 * time.Second
	pingPeriod := 5 * time.Second
	w.conn.SetReadDeadline(time.Now().Add(pongWait))
	w.conn.SetPongHandler(func(string) error {
		w.conn.SetReadDeadline(time.Now().Add(pongWait))
		log.Println("client: pong received")

		return nil
	})

	ticker := time.NewTicker(pingPeriod)

	defer func() {
		ticker.Stop()
		w.conn.Close()
	}()

	go func(){
		for {
			t, msg, err := w.conn.ReadMessage()
			if err != nil {
				log.Println(err.Error())
				w.conn.Close()
				w.broadcast.Write([]byte("exit-"+w.id))

				return
			}

			if t != websocket.TextMessage {
				msg := Message{
					Type: MSG_ERROR,
					Message: "unrecognized data type",
				}

				msgJson, err := json.Marshal(msg)
				if err != nil {
					log.Println(err.Error())
				}
				err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
				if err != nil {
					exit <- true
					return
				}
			} else {
				w.execCommand(msg)
			}
		}
	}()

	go func(){
		for {
			msgRaw := w.broadcast.Read()
			if string(msgRaw) == "exit-"+w.id {
				return
			}

			msg := Message{}
			err := json.Unmarshal(msgRaw, &msg)
			if err != nil {
				continue
			}

			if msg.Data.VideoId != w.videoId {
				continue
			}

			w.mux.Lock()
			err = w.conn.WriteMessage(websocket.TextMessage, msgRaw)
			if err != nil {
				log.Println(err.Error())					
				w.mux.Unlock()
				return
			}
			
			w.mux.Unlock()
		}
	}()

	for {
		select {
		case <- ticker.C:
			w.mux.Lock()
			log.Println("write ping to client")
			if err := w.conn.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				log.Println(err.Error())
				w.mux.Unlock()

				return
			}
			
			w.mux.Unlock()

		case <- exit:
			return

		case <- w.exit:
			return
		}
	}
}

func (w *Worker) execCommand(d []byte) {
	command := Command{}
	err := json.Unmarshal(d, &command)
	if err != nil {
		log.Println(err.Error())
		return
	}

	if command.Command == LIKE_VIDEO {
		if command.VideoId == "" {
			msg := Message{
				Tag: command.Tag,
				Type: MSG_ERROR,
				Message: "videoId required",
			}

			msgJson, err := json.Marshal(msg)
			if err != nil {
				log.Println(err.Error())
			}

			w.mux.Lock()
			err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
			if err != nil {
				log.Println(err.Error())
				w.conn.Close()
				w.mux.Unlock()
				w.exit <- true
				return
			}

			return
		}
		
		go w.likeVideo(command)
	}

	if command.Command == DISLIKE_VIDEO {
		if command.VideoId == "" {
			msg := Message{
				Tag: command.Tag,
				Type: MSG_ERROR,
				Message: "videoId required",
			}

			msgJson, err := json.Marshal(msg)
			if err != nil {
				log.Println(err.Error())
			}

			w.mux.Lock()
			err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
			if err != nil {
				log.Println(err.Error())
				w.conn.Close()
				w.mux.Unlock()
				w.exit <- true
				return
			}

			return
		}
		
		go w.disLikeVideo(command)	
	}

	if command.Command == LIKE_COMMENT {
		if command.CommentId == "" {
			msg := Message{
				Tag: command.Tag,
				Type: MSG_ERROR,
				Message: "commentId required",
			}

			msgJson, err := json.Marshal(msg)
			if err != nil {
				log.Println(err.Error())
			}

			w.mux.Lock()
			err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
			if err != nil {
				log.Println(err.Error())
				w.conn.Close()
				w.mux.Unlock()
				w.exit <- true
				return
			}

			return
		}

		go w.likeComment(command)
	}

	if command.Command == DISLIKE_COMMENT {
		if command.CommentId == "" {
			msg := Message{
				Tag: command.Tag,
				Type: MSG_ERROR,
				Message: "commentId required",
			}

			msgJson, err := json.Marshal(msg)
			if err != nil {
				log.Println(err.Error())
			}

			w.mux.Lock()
			err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
			if err != nil {
				log.Println(err.Error())
				w.conn.Close()
				w.mux.Unlock()
				w.exit <- true
				return
			}

			return
		}

		go w.disLikeComment(command)
	}

	if command.Command == UPDATE_COMMENT {
		if command.CommentId == "" {
			msg := Message{
				Tag: command.Tag,
				Type: MSG_ERROR,
				Message: "commentId required",
			}

			msgJson, err := json.Marshal(msg)
			if err != nil {
				log.Println(err.Error())
			}

			w.mux.Lock()
			err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
			if err != nil {
				log.Println(err.Error())
				w.conn.Close()
				w.mux.Unlock()
				w.exit <- true
				return
			}

			return
		}

		go w.updateComment(command)
	}

	if command.Command == CREATE_COMMENT {
		if command.VideoId == "" {
			msg := Message{
				Tag: command.Tag,
				Type: MSG_ERROR,
				Message: "videoId required",
			}

			msgJson, err := json.Marshal(msg)
			if err != nil {
				log.Println(err.Error())
			}

			w.mux.Lock()
			err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
			if err != nil {
				log.Println(err.Error())
				w.conn.Close()
				w.mux.Unlock()
				w.exit <- true
				return
			}

			return
		}

		go w.createComment(command)
	}
}

func (w *Worker) likeVideo(comm Command) {
	likeCount := ""
	getLikeCount := w.DB.Conn["bagidata"].Table("video").
		Select("like_count").
		Where("video_id", "=", comm.VideoId).
		GetRow()
	err := getLikeCount.Scan(&likeCount)
	if err != nil {
		msg := Message {
			Tag: comm.Tag,
			Type: MSG_INTERNAL_ERROR,
			Message: err.Error(),
		}
		msgJson, err := json.Marshal(msg)
		if err != nil {
			log.Println(err.Error())
			return
		}

		w.mux.Lock()
		err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
		if err != nil {
			log.Println(err.Error())
			w.conn.Close()
			w.mux.Unlock()
			w.exit <- true

			return
		}
		w.mux.Unlock()

		return
	}	

	likeCountI, err := strconv.Atoi(likeCount)
	if err != nil {
		msg := Message {
			Tag: comm.Tag,
			Type: MSG_INTERNAL_ERROR,
			Message: err.Error(),
		}
		msgJson, err := json.Marshal(msg)
		if err != nil {
			log.Println(err.Error())
			return
		}

		w.mux.Lock()
		err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
		if err != nil {
			log.Println(err.Error())
			w.conn.Close()
			w.mux.Unlock()
			w.exit <- true

			return
		}
		w.mux.Unlock()

		return
	}

	likeCountI++
	likeCount = strconv.Itoa(likeCountI)

	updateVideo := w.DB.Conn["bagidata"].Table("video").
	Where("video_id", "=", comm.VideoId).
	Update([][]string{
		{"like_count", likeCount},
	})
	if updateVideo.Error != nil {
		msg := Message {
			Tag: comm.Tag,
			Type: MSG_INTERNAL_ERROR,
			Message: updateVideo.Error.Error(),
		}
		msgJson, err := json.Marshal(msg)
		if err != nil {
			log.Println(updateVideo.Error.Error())
			return
		}

		w.mux.Lock()
		err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
		if err != nil {
			log.Println(err.Error())
			w.conn.Close()
			w.mux.Unlock()
			w.exit <- true

			return
		}
		w.mux.Unlock()

		return
	}

	msg := Message {
		Tag: comm.Tag,
		Type: MSG_INFO,
		Message: "OK",
	}

	msgJson, err := json.Marshal(msg)
	if err != nil {
		log.Println(err.Error())
		return
	}

	w.mux.Lock()
	err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
	if err != nil {
		log.Println(err.Error())
		w.conn.Close()
		w.mux.Unlock()
		w.exit <- true

		return
	}
	w.mux.Unlock()

	msgToAll := Message{
		Type: MSG_LIKE_VIDEO,
		Data: &MessageData{
			VideoId: comm.VideoId,
			LikeCount: likeCount,
		},
	}

	msgToAllJson, err := json.Marshal(msgToAll)
	if err != nil {
		log.Println(err.Error())
		return
	}

	w.broadcast.Write(msgToAllJson)
}

func (w *Worker) disLikeVideo(comm Command) {
	likeCount := ""
	getLikeCount := w.DB.Conn["bagidata"].Table("video").
		Select("dislike_count").
		Where("video_id", "=", comm.VideoId).
		GetRow()
	err := getLikeCount.Scan(&likeCount)
	if err != nil {
		msg := Message {
			Tag: comm.Tag,
			Type: MSG_INTERNAL_ERROR,
			Message: err.Error(),
		}
		msgJson, err := json.Marshal(msg)
		if err != nil {
			log.Println(err.Error())
			return
		}

		w.mux.Lock()
		err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
		if err != nil {
			log.Println(err.Error())
			w.conn.Close()
			w.mux.Unlock()
			w.exit <- true

			return
		}
		w.mux.Unlock()

		return
	}

	likeCountI, err := strconv.Atoi(likeCount)
	if err != nil {
		msg := Message {
			Tag: comm.Tag,
			Type: MSG_INTERNAL_ERROR,
			Message: err.Error(),
		}
		msgJson, err := json.Marshal(msg)
		if err != nil {
			log.Println(err.Error())
			return
		}

		w.mux.Lock()
		err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
		if err != nil {
			log.Println(err.Error())
			w.conn.Close()
			w.mux.Unlock()
			w.exit <- true

			return
		}
		w.mux.Unlock()

		return
	}

	likeCountI++
	likeCount = strconv.Itoa(likeCountI)

	updateVideo := w.DB.Conn["bagidata"].Table("video").
	Where("video_id", "=", comm.VideoId).
	Update([][]string{
		{"dislike_count", likeCount},
	})
	if updateVideo.Error != nil {
		msg := Message {
			Tag: comm.Tag,
			Type: MSG_INTERNAL_ERROR,
			Message: updateVideo.Error.Error(),
		}
		msgJson, err := json.Marshal(msg)
		if err != nil {
			log.Println(updateVideo.Error.Error())
			return
		}

		w.mux.Lock()
		err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
		if err != nil {
			log.Println(err.Error())
			w.conn.Close()
			w.mux.Unlock()
			w.exit <- true

			return
		}
		w.mux.Unlock()

		return
	}

	msg := Message {
		Tag: comm.Tag,
		Type: MSG_INFO,
		Message: "OK",
	}

	msgJson, err := json.Marshal(msg)
	if err != nil {
		log.Println(err.Error())
		return
	}

	w.mux.Lock()
	err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
	if err != nil {
		log.Println(err.Error())
		w.conn.Close()
		w.mux.Unlock()
		w.exit <- true

		return
	}
	w.mux.Unlock()

	msgToAll := Message{
		Type: MSG_DISLIKE_VIDEO,
		Data: &MessageData{
			VideoId: comm.VideoId,
			DislikeCount: likeCount,
		},
	}

	msgToAllJson, err := json.Marshal(msgToAll)
	if err != nil {
		log.Println(err.Error())
		return
	}

	w.broadcast.Write(msgToAllJson)
}

func (w *Worker) likeComment(comm Command) {
	likeCount := ""
	getLikeCount := w.DB.Conn["bagidata"].Table("comment").
		Select("like_count").
		Where("comment_id", "=", comm.CommentId).
		GetRow()
	err := getLikeCount.Scan(&likeCount)
	if err != nil {
		msg := Message {
			Tag: comm.Tag,
			Type: MSG_INTERNAL_ERROR,
			Message: err.Error(),
		}
		msgJson, err := json.Marshal(msg)
		if err != nil {
			log.Println(err.Error())
			return
		}

		w.mux.Lock()
		err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
		if err != nil {
			log.Println(err.Error())
			w.conn.Close()
			w.mux.Unlock()
			w.exit <- true

			return
		}
		w.mux.Unlock()

		return
	}

	likeCountI, err := strconv.Atoi(likeCount)
	if err != nil {
		msg := Message {
			Tag: comm.Tag,
			Type: MSG_INTERNAL_ERROR,
			Message: err.Error(),
		}
		msgJson, err := json.Marshal(msg)
		if err != nil {
			log.Println(err.Error())
			return
		}

		w.mux.Lock()
		err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
		if err != nil {
			log.Println(err.Error())
			w.conn.Close()
			w.mux.Unlock()
			w.exit <- true

			return
		}
		w.mux.Unlock()

		return
	}

	likeCountI++
	likeCount = strconv.Itoa(likeCountI)

	updateVideo := w.DB.Conn["bagidata"].Table("comment").
	Where("comment_id", "=", comm.CommentId).
	Update([][]string{
		{"like_count", likeCount},
	})
	if updateVideo.Error != nil {
		msg := Message {
			Tag: comm.Tag,
			Type: MSG_INTERNAL_ERROR,
			Message: updateVideo.Error.Error(),
		}
		msgJson, err := json.Marshal(msg)
		if err != nil {
			log.Println(updateVideo.Error.Error())
			return
		}

		w.mux.Lock()
		err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
		if err != nil {
			log.Println(err.Error())
			w.conn.Close()
			w.mux.Unlock()
			w.exit <- true

			return
		}
		w.mux.Unlock()

		return
	}	
	
	msg := Message {
		Tag: comm.Tag,
		Type: MSG_INFO,
		Message: "OK",
	}

	msgJson, err := json.Marshal(msg)
	if err != nil {
		log.Println(err.Error())
		return
	}

	w.mux.Lock()
	err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
	if err != nil {
		log.Println(err.Error())
		w.conn.Close()
		w.mux.Unlock()
		w.exit <- true

		return
	}
	w.mux.Unlock()

	msgToAll := Message{
		Type: MSG_LIKE_COMMENT,
		Data: &MessageData{
			VideoId: comm.VideoId,
			LikeCount: likeCount,
		},
	}

	msgToAllJson, err := json.Marshal(msgToAll)
	if err != nil {
		log.Println(err.Error())
		return
	}

	w.broadcast.Write(msgToAllJson)
}

func (w *Worker) disLikeComment(comm Command) {
	likeCount := ""
	getLikeCount := w.DB.Conn["bagidata"].Table("comment").
		Select("dislike_count").
		Where("comment_id", "=", comm.CommentId).
		GetRow()
	err := getLikeCount.Scan(&likeCount)
	if err != nil {
		msg := Message {
			Tag: comm.Tag,
			Type: MSG_INTERNAL_ERROR,
			Message: err.Error(),
		}
		msgJson, err := json.Marshal(msg)
		if err != nil {
			log.Println(err.Error())
			return
		}

		w.mux.Lock()
		err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
		if err != nil {
			log.Println(err.Error())
			w.conn.Close()
			w.mux.Unlock()
			w.exit <- true

			return
		}
		w.mux.Unlock()

		return
	}

	likeCountI, err := strconv.Atoi(likeCount)
	if err != nil {
		msg := Message {
			Tag: comm.Tag,
			Type: MSG_INTERNAL_ERROR,
			Message: err.Error(),
		}
		msgJson, err := json.Marshal(msg)
		if err != nil {
			log.Println(err.Error())
			return
		}

		w.mux.Lock()
		err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
		if err != nil {
			log.Println(err.Error())
			w.conn.Close()
			w.mux.Unlock()
			w.exit <- true

			return
		}
		w.mux.Unlock()

		return
	}

	likeCountI++
	likeCount = strconv.Itoa(likeCountI)

	updateVideo := w.DB.Conn["bagidata"].Table("comment").
	Where("comment_id", "=", comm.CommentId).
	Update([][]string{
		{"dislike_count", likeCount},
	})
	if updateVideo.Error != nil {
		msg := Message {
			Tag: comm.Tag,
			Type: MSG_INTERNAL_ERROR,
			Message: updateVideo.Error.Error(),
		}
		msgJson, err := json.Marshal(msg)
		if err != nil {
			log.Println(updateVideo.Error.Error())
			return
		}

		w.mux.Lock()
		err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
		if err != nil {
			log.Println(err.Error())
			w.conn.Close()
			w.mux.Unlock()
			w.exit <- true

			return
		}
		w.mux.Unlock()

		return
	}

	msg := Message {
		Tag: comm.Tag,
		Type: MSG_INFO,
		Message: "OK",
	}

	msgJson, err := json.Marshal(msg)
	if err != nil {
		log.Println(err.Error())
		return
	}

	w.mux.Lock()
	err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
	if err != nil {
		log.Println(err.Error())
		w.conn.Close()
		w.mux.Unlock()
		w.exit <- true

		return
	}
	w.mux.Unlock()

	msgToAll := Message{
		Type: MSG_DISLIKE_COMMENT,
		Data: &MessageData{
			VideoId: comm.VideoId,
			DislikeCount: likeCount,
		},
	}

	msgToAllJson, err := json.Marshal(msgToAll)
	if err != nil {
		log.Println(err.Error())
		return
	}

	w.broadcast.Write(msgToAllJson)
}

func (w *Worker) updateComment(comm Command) {
	updateComment := w.DB.Conn["bagidata"].Table("comment").
		Where("comment_id", "=", comm.CommentId).
		Update([][]string{
			{"comment_text", comm.Data},
		})
	if updateComment.Error != nil {
		msg := Message {
			Tag: comm.Tag,
			Type: MSG_INTERNAL_ERROR,
			Message: updateComment.Error.Error(),
		}
		msgJson, err := json.Marshal(msg)
		if err != nil {
			log.Println(updateComment.Error.Error())
			return
		}

		w.mux.Lock()
		err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
		if err != nil {
			log.Println(err.Error())
			w.conn.Close()
			w.mux.Unlock()
			w.exit <- true

			return
		}
		w.mux.Unlock()

		return
	}	

	msg := Message {
		Tag: comm.Tag,
		Type: MSG_INFO,
		Message: "OK",
	}

	msgJson, err := json.Marshal(msg)
	if err != nil {
		log.Println(err.Error())
		return
	}

	w.mux.Lock()
	err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
	if err != nil {
		log.Println(err.Error())
		w.conn.Close()
		w.mux.Unlock()
		w.exit <- true

		return
	}
	w.mux.Unlock()

	msgToAll := Message{
		Type: MSG_UPDATE_COMMENT,
		Data: &MessageData{
			VideoId: comm.VideoId,
			CommentId: comm.CommentId,
			UpdatedText: comm.Data,
		},
	}

	msgToAllJson, err := json.Marshal(msgToAll)
	if err != nil {
		log.Println(err.Error())
		return
	}

	w.broadcast.Write(msgToAllJson)
}

func (w *Worker) createComment(comm Command) {
	commId := util.RandString(20)
	insertComment := w.DB.Conn["bagidata"].Table("comment").		
		Insert([][]string{
			{"comment_id", commId},
			{"user_id", w.id},
			{"video_id", comm.VideoId},
			{"comment_text", comm.Data},			
		})
	if insertComment.Error != nil {
		msg := Message {
			Tag: comm.Tag,
			Type: MSG_INTERNAL_ERROR,
			Message: insertComment.Error.Error(),
		}
		msgJson, err := json.Marshal(msg)
		if err != nil {
			log.Println(insertComment.Error.Error())
			return
		}

		w.mux.Lock()
		err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
		if err != nil {
			log.Println(err.Error())
			w.conn.Close()
			w.mux.Unlock()
			w.exit <- true

			return
		}
		w.mux.Unlock()

		return
	}	

	msg := Message {
		Tag: comm.Tag,
		Type: MSG_INFO,
		Message: "OK",
	}

	msgJson, err := json.Marshal(msg)
	if err != nil {
		log.Println(err.Error())
		return
	}

	w.mux.Lock()
	err = w.conn.WriteMessage(websocket.TextMessage, msgJson)
	if err != nil {
		log.Println(err.Error())
		w.conn.Close()
		w.mux.Unlock()
		w.exit <- true

		return
	}
	w.mux.Unlock()

	msgToAll := Message{
		Type: MSG_CREATE_COMMENT,
		Data: &MessageData{
			VideoId: comm.VideoId,
			CommentId: commId,
			Text: comm.Data,
		},
	}

	msgToAllJson, err := json.Marshal(msgToAll)
	if err != nil {
		log.Println(err.Error())
		return
	}

	w.broadcast.Write(msgToAllJson)
}