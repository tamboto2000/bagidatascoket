package session

import (
	"sync"
)

type Session struct {
	sess map[string]string
	mux *sync.Mutex
}

func (ses *Session) Add(p, v string) {
	ses.mux.Lock()
	ses.sess[p] = v
	ses.mux.Unlock()
}

func (ses *Session) Delete(p string) {
	ses.mux.Lock()
	delete(ses.sess, p)
	ses.mux.Unlock()
}

func (ses *Session) Check(p string) bool {
	_, i := ses.sess[p]
	if i {
		return true
	}

	return false
}

func (ses *Session) Get(p string) string {
	return ses.sess[p]
}

func New() *Session {
	return &Session{
		sess: make(map[string]string),
		mux: &sync.Mutex{},
	}
}