package main

import (		
	"collin/spider/util"		
	"net/http"		
	"net/url"			
	"log"
	"fmt"

	"github.com/gorilla/websocket"
)

func main() {
	u, err := url.Parse("ws://31.220.61.116:8000/twitter/connect/client")
	if err != nil {
		panic(err.Error())
	}

	log.Printf("connecting to %s", u.String())

	header := http.Header{}	
	header.Add("client-id", util.RandString(20))

	c, _, err := websocket.DefaultDialer.Dial(u.String(), header)
	
	if err != nil {
		log.Fatal("dial:", err)
	}

	c.SetPingHandler(func(msg string) error {
		log.Println("ping received")
		err := c.WriteMessage(websocket.PongMessage, []byte{})

		return err
	})

	defer c.Close()

	for {
		_, message, err := c.ReadMessage()
		if err != nil {
			log.Println("read:", err)

			return
		}

		fmt.Println(string(message))
	}	
}