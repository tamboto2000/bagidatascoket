package main

import (		
	"collin/spider/util"
	"encoding/json"
	"collin/mysql"	
	"net/http"		
	"net/url"		
	"strconv"
	"log"
	"fmt"

	"github.com/gorilla/websocket"
)

var dbBuild mysql.Build

type Message struct {
	Type string `json:"type"`
	From string `json:"from"`
	Target string `json:"target,omitempty"`
	Data string `json:"data,omitempty"`
}

type Tweet struct {
	Index *Index `json:"index"`
	Media *Media `json:"media"`
	Location *Location `json:"location"`
	Retweet *Retweet `json:"retweet"`
}

type Location struct {
	UserId string `json:"userId"`
	PostId string `json:"postId"`
	LongVal string `json:"longVal"`
	LatVal string `json:"latVal"`
}

type Media struct {
	UserId string `json:"userId"`
	PostId string `json:"postId"`
	MediaURL string `json:"mediaUrl"`
	ItemURL interface{} `json:"itemUrl"`
	MediaType string `json:"mediaType"`
	CreatedAt string `json:"createdAt"`
	UpdatedAt string `json:"updatedAt"`
}

type Retweet struct {
	UserId string `json:"userId"`
	PostId string `json:"postId"`
	OriPostId string `json:"oriPostId"`
	OriOwner string `json:"oriOwner"`
	TweetText string `json:"tweetText"`
	LikeCount interface{} `json:"likeCount"`
	CommentCount interface{} `json:"commentCount"`
	RetweetCount interface{} `json:"retweetCount"`
	TwtrCreatedAt string `json:"twtrCreatedAt"`	
	CreatedAt string `json:"createdAt"`
	UpdatedAt string `json:"updatedAt"`
}

type Index struct {
	UserId string `json:"userId"`
	PostId string `json:"postId"`	
	TweetText string `json:"tweetText"`
	LikeCount interface{} `json:"likeCount"`
	CommentCount interface{} `json:"commentCount"`
	RetweetCount interface{} `json:"retweetCount"`
	TwtrCreatedAt string `json:"twtrCreatedAt"`	
	CreatedAt string `json:"createdAt"`
	UpdatedAt string `json:"updatedAt"`
}

func main() {
	db, err := dbBuild.Build(mysql.Config{
		Tag:      "user",
		Host:     "127.0.0.1",
		Name:     "data_lake_bagidata",
		Username: "root",
		Password: "YebRZcf7ym",
		// Password: "kepler22b",
		Collation: "utf8mb4_unicode_ci",
		MaxAllowedPacket: "0",
	})	
	
	u, err := url.Parse("ws://31.220.61.116:8000/twitter/connect/client")
	if err != nil {
		panic(err.Error())
	}

	log.Printf("connecting to %s", u.String())

	header := http.Header{}	
	header.Add("client-id", util.RandString(20))

	c, _, err := websocket.DefaultDialer.Dial(u.String(), header)
	
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	go func() {	
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Println("read:", err)

				return
			}

			data := Message{}
			err = json.Unmarshal(message, &data)
			if err != nil {
				log.Println(err.Error())
			}

			tweet := Tweet{}
			err = json.Unmarshal([]byte(data.Data), &tweet)
			if err != nil {
				log.Println(err.Error())
			}

			insertToDB(tweet, db)
			
			fmt.Println(string(message))
		}
	}()

	select{}
}

func insertToDB (tweet Tweet, db mysql.Build) {
	//Insert index
	index := tweet.Index

	insertQuery := [][]string{			
		{"user_id", index.UserId},
		{"post_id", index.PostId},
		{"tweet_text", index.TweetText},
		{"twtr_created_at", index.TwtrCreatedAt},
		{"created_at", index.CreatedAt},
		{"updated_at", index.UpdatedAt},
		{"extended", "1"},
	}

	if index.LikeCount != nil {
		insertQuery = append(insertQuery, []string{"like_count", strconv.FormatFloat(index.LikeCount.(float64), 'f', 0, 64)})
	}
	
	if index.CommentCount != nil {
		insertQuery = append(insertQuery, []string{"comment_count", strconv.FormatFloat(index.CommentCount.(float64), 'f', 0, 64)})
	}

	if index.RetweetCount != nil {
		insertQuery = append(insertQuery, []string{"retweet_count", strconv.FormatFloat(index.RetweetCount.(float64), 'f', 0, 64)})
	}

	insert := db.Table("twitter_tweets_index_hilmi").Insert(insertQuery)
	if insert.Error != nil {
		log.Println(insert.Error.Error())
	}

	//Insert media		
	if tweet.Media != nil {
		media := tweet.Media

		insertQuery := [][]string{
			{"user_id", media.UserId},
			{"post_id", media.PostId},
			{"media_url", media.MediaURL},
			{"media_type", media.MediaType},
			{"created_at", media.CreatedAt},
			{"updated_at", media.UpdatedAt},
		}

		if media.ItemURL != nil {
			insertQuery = append(insertQuery, []string{"item_url", media.ItemURL.(string)})
		}

		insert := db.Table("twitter_tweet_media_hilmi").Insert(insertQuery)
		if insert.Error != nil {
			log.Println(insert.Error.Error())
		}
	}

	if tweet.Location != nil {
		location := tweet.Location

		insertQuery := [][]string{
			{"user_id", location.UserId},
			{"post_id", location.PostId},
			{"long_val", location.LongVal},
			{"lat_val", location.LatVal},
		}

		insert := db.Table("twitter_tweet_location_hilmi").Insert(insertQuery)
		if insert.Error != nil {
			log.Println(insert.Error.Error())
		}
	}

	if tweet.Retweet != nil {
		index := tweet.Retweet

		insertQuery := [][]string{			
			{"user_id", index.UserId},
			{"post_id", index.PostId},
			{"ori_post_id", index.OriPostId},
			{"ori_owner", index.OriOwner},
			{"tweet_text", index.TweetText},
			{"twtr_created_at", index.TwtrCreatedAt},
			{"created_at", index.CreatedAt},
			{"updated_at", index.UpdatedAt},			
		}

		if index.LikeCount != nil {
			insertQuery = append(insertQuery, []string{"like_count", strconv.FormatFloat(index.LikeCount.(float64), 'f', 0, 64)})
		}
		
		if index.CommentCount != nil {
			insertQuery = append(insertQuery, []string{"comment_count", strconv.FormatFloat(index.CommentCount.(float64), 'f', 0, 64)})
		}
	
		if index.RetweetCount != nil {
			insertQuery = append(insertQuery, []string{"retweet_count", strconv.FormatFloat(index.RetweetCount.(float64), 'f', 0, 64)})
		}

		insert := db.Table("twitter_tweet_retweeted_hilmi").Insert(insertQuery)
		if insert.Error != nil {
			log.Println(insert.Error.Error())
		}
	}
}