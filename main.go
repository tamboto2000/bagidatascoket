package main

import (	
	"github.com/gorilla/mux"
	"collin/broadcast"
	"collin/mysql"
	"net/http"
	"runtime"
	"log"
		
	"BagiDataSocket/controller/websocket"	
	"BagiDataSocket/database"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU() + 1)

	// sess := session.New()
	db := database.New()
	db.Add(mysql.Config{
		Tag:      "bagidata",
		Host:     "127.0.0.1",
		Name:     "bagidata_test",
		Username: "root",
		// Password: "kepler22b",
		Password: "t%5pg&RdNqw*CBRxe7Vi1ah",
	})

	broadcast := broadcast.NewBroadcast()
	broadcast.Start()
	socket := &websocket.Socket {
		DB: db,
		Broadcast: broadcast,
	}
		
	r := mux.NewRouter()
	
	r.HandleFunc("/socket/connect", socket.Connect).Methods("GET")		
	r.HandleFunc("/video/data", socket.VideoData).Methods("GET")

	log.Fatal(http.ListenAndServe(":8080", r))
}