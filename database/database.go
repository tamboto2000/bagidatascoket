package database

import (
	"collin/mysql"
	"strings"
)

type DB struct {
	Conn map[string]mysql.Build
}

func New() *DB {
	db := &DB{}

	db.Conn = make(map[string]mysql.Build)

	return db
}

func (db *DB) Add(conf mysql.Config) {
	dbInit := mysql.Build{}

	if conf.Tag == "" || strings.Contains(conf.Tag, " ") {
		panic("database tag is invalid")
	}

	conn, err := dbInit.Build(conf)

	if err != nil {
		panic(err.Error())
	}

	db.Conn[conf.Tag] = conn
}

