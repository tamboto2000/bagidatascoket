create table video (
    id int not null primary key auto_increment,
    video_id char(255),
    title text not null,
    file_dir text not null,
    description text not null,
    like_count int default 0,
    dislike_count int default 0,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp on update current_timestamp
);

create table comment (
    id int not null primary key auto_increment,
    comment_id char(255),
    user_id char(255),
    video_id char(255),
    comment_text text,
    like_count int default 0,
    dislike_count int default 0,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp on update current_timestamp
);