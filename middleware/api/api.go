package api

import (	
	"collin/spider/util"
	"net/http"	

	"data-pipeline/database"
	"data-pipeline/session"
)

type API struct {
	DB *database.DB
	Session *session.Session
}

func (ln *API) API(next http.Handler) http.Handler {	
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        tokenId := r.Header.Get("token-id")
		token := r.Header.Get("token")	

		if tokenId == "" || token == "" {
			http.Error(w, "bad request", 400)
			return
		}

		if !ln.Session.Check(tokenId) {
			http.Error(w, "token invalid", 403)
			return
		}	

		enc_token := ln.Session.Get(tokenId)
		if _, err := util.AESDecrypt(enc_token, token); err != nil {
			http.Error(w, "token invalid", 403)
			return
		} else {
			next.ServeHTTP(w, r)
			return
		}
    })	
}