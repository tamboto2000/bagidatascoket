package main

import (
	"flag"
	"log"
	"encoding/json"
	"net/url"	
	"net/http"	
	"collin/spider/util"
	"bufio"
	"fmt"
	"os"

	"github.com/gorilla/websocket"
)

type Command struct {
	Tag string `json:"tag"`
	VideoId string `json:"videoId"`
	CommentId string `json:"commentId"`
	Command string `json:"command"`
	Data string `json:"data"`
}

type Message struct {
	Tag string `json:"tag,omitempty"`
	Type string `json:"type,omitempty"`
	Message string `json:"message,omitempty"`
	Data *MessageData `json:"data,omitempty"`
}

type MessageData struct {
	VideoId string `json:"videoId,omitempty"`
	CommentId string `json:"commentId,omitempty"`
	LikeCount string `json:"likeCount,omitempty"`
	DislikeCount string `json:"disLikeCount,omitempty"`
	UpdatedText string `json:"updatedText,omitempty"`
}

const videoId = "1234555"

func main() {
	flag.Parse()
	log.SetFlags(0)	
		
	u, err := url.Parse("ws://31.220.61.116:8080/socket/connect?videoId="+videoId)
	if err != nil {
		panic(err.Error())
	}

	log.Printf("connecting to %s", u.String())

	header := http.Header{}	
	header.Add("id", util.RandString(20))

	c, _, err := websocket.DefaultDialer.Dial(u.String(), header)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()
	log.Println("connected\n")

	go func() {
		for {
			_, msg, err := c.ReadMessage()
			if err != nil {
				log.Println("read:", err)

				return
			}
			
			message := Message{}
			err = json.Unmarshal(msg, &message)
			if err != nil {
				log.Println(err.Error())
				os.Exit(1)
			}

			if message.Type == "error" || message.Type == "internal-error" {
				log.Println(message.Message)
				fmt.Print("> ")
			} else {
				log.Println("OK")
				fmt.Print("> ")
			}
		}
	}()	

	fmt.Println("command list:")
	fmt.Println("[1] likeVideo\n[2] dislikeVideo\n[3] likeComment\n[4] dislikeComment\n[5] updateComment\n[6] createComment\n[exit] exit from program")
	for {		
		fmt.Print("> ")
		comm := scanner()

		if comm == "1" {
			likeVideo(c)
		} else if comm == "2" {
			dislikeVideo(c)
		} else if comm == "3" {
			likeComment(c)
		} else if comm == "4" {
			dislikeComment(c)
		} else if comm == "5" {
			updateComment(c)
		} else if comm == "6" {
			createComment(c)
		} else if comm == "exit" {
			os.Exit(0)
		} else {
			fmt.Println("command is not recognized")
		}
	}
}

func likeVideo(c *websocket.Conn) {
	comm := Command{
		Tag: "likeVideo",
		VideoId: videoId,
		Command: "like-video",
	}

	msg, _ := json.Marshal(comm)
	err := c.WriteMessage(websocket.TextMessage, msg)
	if err != nil {
		log.Println(err.Error())
	}
}

func dislikeVideo(c *websocket.Conn) {
	comm := Command{
		Tag: "dislikeVideo",
		VideoId: videoId,
		Command: "dislike-video",
	}

	msg, _ := json.Marshal(comm)
	err := c.WriteMessage(websocket.TextMessage, msg)
	if err != nil {
		log.Println(err.Error())
	}
}

func likeComment(c *websocket.Conn) {
	comm := Command{
		Command: "like-comment",
		Tag: "likeComment",
		VideoId: videoId,
		CommentId: "1234",
	}

	msg, _ := json.Marshal(comm)
	err := c.WriteMessage(websocket.TextMessage, msg)
	if err != nil {
		log.Println(err.Error())
	}
}

func dislikeComment(c *websocket.Conn) {
	comm := Command{
		Command: "dislike-comment",
		Tag: "dislikeComment",
		VideoId: videoId,
		CommentId: "1234",
	}

	msg, _ := json.Marshal(comm)
	err := c.WriteMessage(websocket.TextMessage, msg)
	if err != nil {
		log.Println(err.Error())
	}
}

func updateComment(c *websocket.Conn) {
	comm := Command{
		Command: "update-comment",
		Tag: "updateComment",
		VideoId: videoId,
		CommentId: "1234",
		Data: "Hai mamank",
	}

	msg, _ := json.Marshal(comm)
	err := c.WriteMessage(websocket.TextMessage, msg)
	if err != nil {
		log.Println(err.Error())
	}
}

func createComment(c *websocket.Conn) {
	comm := Command{
		Command: "create-comment",
		Tag: "createComment",
		VideoId: videoId,
		Data: "tes ya mamank",
	}

	msg, _ := json.Marshal(comm)
	err := c.WriteMessage(websocket.TextMessage, msg)
	if err != nil {
		log.Println(err.Error())
	}
}

func scanner() string {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		return scanner.Text()
	}

	return ""
}