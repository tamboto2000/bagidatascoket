package main

import (
	"flag"
	"log"
	// "encoding/json"
	"net/url"	
	"net/http"	
	"collin/spider/util"
	"fmt"

	"github.com/gorilla/websocket"
)

func main() {
	flag.Parse()
	log.SetFlags(0)	
	
	videoId := "1234555"
	u, err := url.Parse("ws://31.220.61.116:8080/socket/connect?videoId="+videoId)
	if err != nil {
		panic(err.Error())
	}

	log.Printf("connecting to %s", u.String())

	header := http.Header{}	
	header.Add("id", util.RandString(20))

	c, _, err := websocket.DefaultDialer.Dial(u.String(), header)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	go func() {
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Println("read:", err)

				return
			}			

			fmt.Println(string(message))
		}
	}()

	select{}
}